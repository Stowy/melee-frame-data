import React from "react";
import Navigation from "./Navigation/Navigation";
import AppLoading from 'expo-app-loading';
import * as Font from "expo-font";
import { Provider as ReduxProvider } from "react-redux";
import Store from "./Store/configureStore";

let customFonts = {
    FolkProH: require("./assets/fonts/Folk-Pro-H.otf"),
    FolkProB: require("./assets/fonts/Folk-Pro-B.otf"),
    FolkProM: require("./assets/fonts/Folk-Pro-M.otf"),
    FolkProR: require("./assets/fonts/Folk-Pro-R.otf"),
};

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            areFontsLoaded: false,
        };
    }

    async _loadFontsAsync() {
        await Font.loadAsync(customFonts);
        this.setState({ areFontsLoaded: true });
    }

    async componentDidMount() {
        await this._loadFontsAsync();
    }

    render() {
        if (this.state.areFontsLoaded) {
            return (
                <ReduxProvider store={Store}>
                    <Navigation />
                </ReduxProvider>
            );
        } else {
            return <AppLoading />;
        }
    }
}
