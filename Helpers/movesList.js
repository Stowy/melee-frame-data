export const movesList = [
    {
        name: "jab1",
        isNormal: true,
    },
    {
        name: "jab2",
        isNormal: true,
    },
    {
        name: "jab3",
        isNormal: true,
    },
    {
        name: "rapidjabs_start",
        isNormal: true,
    },
    {
        name: "rapidjabs_loop",
        isNormal: true,
    },
    {
        name: "rapidjabs_end",
        isNormal: true,
    },
    {
        name: "dashattack",
        isNormal: true,
    },
    {
        name: "ftilt_h",
        isNormal: true,
    },
    {
        name: "ftilt_mh",
        isNormal: true,
    },
    {
        name: "ftilt_m",
        isNormal: true,
    },
    {
        name: "ftilt_ml",
        isNormal: true,
    },
    {
        name: "ftilt_l",
        isNormal: true,
    },
    {
        name: "utilt",
        isNormal: true,
    },
    {
        name: "dtilt",
        isNormal: true,
    },
    {
        name: "fsmash_h",
        isNormal: true,
    },
    {
        name: "fsmash_mh",
        isNormal: true,
    },
    {
        name: "fsmash_m",
        isNormal: true,
    },
    {
        name: "fsmash_ml",
        isNormal: true,
    },
    {
        name: "fsmash_l",
        isNormal: true,
    },
    {
        name: "usmash",
        isNormal: true,
    },
    {
        name: "dsmash",
        isNormal: true,
    },
    {
        name: "nair",
        isNormal: true,
    },
    {
        name: "bair",
        isNormal: true,
    },
    {
        name: "uair",
        isNormal: true,
    },
    {
        name: "dair",
        isNormal: true,
    },
    {
        name: "grab",
        isNormal: true,
    },
    {
        name: "dashgrab",
        isNormal: true,
    },
    {
        name: "pummel",
        isNormal: true,
    },
    {
        name: "fthrow",
        isNormal: true,
    },
    {
        name: "bthrow",
        isNormal: true,
    },
    {
        name: "uthrow",
        isNormal: true,
    },
    {
        name: "dthrow",
        isNormal: true,
    },
    {
        name: "nspecial",
        isNormal: false,
    },
    {
        name: "nspecial_air",
        isNormal: false,
    },
    {
        name: "sspecial",
        isNormal: false,
    },
    {
        name: "sspecial_air",
        isNormal: false,
    },
    {
        name: "uspecial",
        isNormal: false,
    },
    {
        name: "uspecial_air",
        isNormal: false,
    },
    {
        name: "dspecial",
        isNormal: false,
    },
    {
        name: "dspecial_air",
        isNormal: false,
    },
];
