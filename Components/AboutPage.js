import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import Text from "./Text";
import Anchor from "./Anchor";
import TextHeader from "./TextHeader";

class AboutPage extends React.Component {
    render() {
        return (
            <ScrollView style={styles.main_container}>
                <TextHeader style={styles.title}>Informations</TextHeader>
                <Text>
                    Created by <Anchor href="https://twitter.com/St0wy">@St0wy</Anchor>. Inspired by{" "}
                    <Anchor href="http://meleeframedata.com/">Janoris's meleeframedata.com</Anchor> and{" "}
                    <Anchor href="https://ultimateframedata.com/">ultimateframedata.com</Anchor>. Don't hesitate to
                    tweet me any requests or corrections !
                </Text>
                <Text>
                    {" "}
                    Also, this application is open source. You can check the code on{" "}
                    <Anchor href="https://gitlab.com/Stowy/melee-frame-data">GitLab</Anchor>.
                </Text>
                <TextHeader style={styles.title}>Data sources</TextHeader>
                <Text>
                    <Anchor href="https://github.com/pfirsich/meleeFrameDataExtractor">
                        Joel Schumacher's frame data extractor
                    </Anchor>{" "}
                    and <Anchor href="http://meleeframedata.com/">Janoris's meleeframedata.com</Anchor>. The images are
                    also from meleeframedata.com.
                </Text>
                <TextHeader style={styles.title}>Terminology</TextHeader>
                <Text style={styles.subtitle}>Startup</Text>
                <Text>Refers to the frame the hitbox will begin to be active.</Text>
                <Text style={styles.subtitle}>Active Frames</Text>
                <Text>
                    Refers to the frames of which a hitbox is out. This will refer to multiple hitboxes in multi-hit
                    moves.
                </Text>
                <Text style={styles.subtitle}>Total Frames</Text>
                <Text>
                    Refers to the amount of frames a move has from the beginning of its activation to its final frame of
                    endlag. Sometimes, with projectile moves, this number will be lower than the active end. Just know
                    that once the frame listed is hit, your character will be able to act again.
                </Text>
                <Text style={styles.subtitle}>IASA</Text>
                <Text>
                    Stands for Interruptible As Soon As, which means you can cancel the animation if you input another
                    action at said frame.
                </Text>
                <Text style={styles.subtitle}>Shield Stun</Text>
                <Text>
                    Refers to the amount of time an opponent will be stuck in shield if a character attacks their shield
                    with said move. Every character will incur shield stun except for Yoshi.
                </Text>
                <Text style={styles.subtitle}>Base Damage</Text>
                <Text>
                    Refers to the amount of damage the move will do unstale. Since multi-hit moves are unreliable in
                    this game, I decided to list damage as so: the first number is the amount of damage the strongest
                    hitbox will do, while the second number is the amount of damage the weakest hit will do. So be
                    careful reading these, as the total % damage a move is meant to do may not be the same as what is
                    listed. Also, although there are no decimals in your percent in this game (like in ultimate), moves
                    will still do those tenths of % damage. This means that sometimes, the % for multiple moves won't
                    add up exactly to what will be listed. This is usually only a problem on moves with many hits, but
                    unfortunately, I have not been able to find any data on the exact percentages these moves do, so all
                    %'s listed are rounded to the nearest whole number.
                </Text>
                <Text style={styles.subtitle}>Auto Cancel</Text>
                <Text>
                    Refers to landing with an aerial at a certain time such that no excess landing lag will occur from
                    said aerial.
                </Text>
                <Text style={styles.subtitle}>LFS</Text>
                <Text>
                    Stands for Landing Fall Special, and refers to the amount of frames a character will occur of
                    landing lag if put into special fall by a particular move. Many up b's will incur this lag IF they
                    end high above the stage and fall long enough to enter special fall. If not, they will go through
                    normal landing lag.
                </Text>
                <Text style={styles.subtitle}>Landing Lag</Text>
                <Text>
                    Refers to the lag you experience if you land during the middle of a move that is not in the auto
                    cancel window.
                </Text>
                <Text style={styles.subtitle}>L-Cancel Lag</Text>
                <Text>
                    Refers to the lag you experience if you perform a successful L Cancel while landing with a move (7
                    frame window before landing).
                </Text>
                <Text style={styles.subtitle}>Inv. Frames</Text>
                <Text>Stands for invulnerable frames, where a character can not be damaged/interacted with.</Text>
                <Text style={styles.subtitle}>PLA Intangibility Frames</Text>
                <Text>
                    Stands for Perfect Ledgedash Angle, and it describes the amount of intangibility frames a character
                    will have to act with if a perfect ledgedash was performed (on Final Destination ledges).
                </Text>
                <Text style={styles.subtitle}>Staling Moves</Text>
                <Text>
                    It's a mechanic that will change the damage and shieldstun a move will deal when that move is used
                    multiple times. Numbers provided on this site refer to the base damage and shieldstun a move will do
                    if it is not stale.
                </Text>

                <TextHeader style={styles.title}>Universal data</TextHeader>
                <Text style={styles.subtitle}>Power Shield</Text>
                <Text>
                    To Power Shield projectiles, start shielding 2 frames before the projectile hits you. To power
                    shield normal attacks, start shielding 4 frames before the attack hits you. Powershielding is useful
                    because it will not deplete a character's shield and will decrease the amount of shieldstun a
                    character receives (ONLY IF USING GROUNDED ATTACKS). Power shielding projectiles reflects them back
                    at the opponent, but reduces their damage by half and lowers knockback.
                </Text>
                <Text style={styles.subtitle}> Dropping Shield </Text>
                <Text>
                    Dropping Shield takes 15 frames. Jumping and grabbing out of shield does not incur shield drop.
                </Text>
                <Text style={styles.subtitle}> Jump Canceling </Text>
                <Text>
                    Refers to inputting an up smash, up special, grab, or item throw during a character's jump squat
                    animation to cancel the jump and perform said move. Useful for characters to act quickly out of
                    shield (except Yoshi, who can not jump out of shield), or to use a standing grab instead of a dash
                    grab (to avoid extra frames of lag).{" "}
                    <Anchor href="https://smashboards.com/threads/out-of-shield-options-frame-data.294336/">
                        This thread
                    </Anchor>{" "}
                    goes into detail about each character's best out of shield options
                </Text>
                <Text style={styles.subtitle}>Throws</Text>
                <Text>
                    Throws have an interesting property where the amount of frames a throw takes to complete will depend
                    on the weight of the character being thrown. The frame time window listed on this site are defaulted
                    to throw Mario (a mid-weight), but I plan on adding info for how much these windows will change
                    based on character weight in the future.
                </Text>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        padding: 10,
    },
    title: {
        fontSize: 18,
        marginBottom: 5,
        marginTop: 15,
    },
    subtitle: {
        fontSize: 14,
        marginBottom: 5,
        marginTop: 10,
        fontFamily: "FolkProB"
    },
});

export default AboutPage;
