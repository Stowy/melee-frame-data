import React from "react";
import { Text as PaperText } from "react-native-paper";

/**
 * Custom text component to use the melee font by default.
 */
class Text extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <PaperText {...this.props} style={[{ fontFamily: "FolkProM" }, this.props.style]}>
                {this.props.children}
            </PaperText>
        );
    }
}

export default Text;
