import React from "react";
import Text from "./Text";
import { connect } from "react-redux";
import { View, StyleSheet } from "react-native";
import generalMovesName from "../Helpers/generalMovesName.json";
import movesName from "../Helpers/movesName.json";

class SpecialMoveInformations extends React.Component {
    constructor(props) {
        super(props);
    }

    /**
     * Computes the shield stun for the provided damage.
     * @param {number} damage Damage that the move deals.
     * @returns {number}
     */
    _computeShieldStun(damage) {
        return Math.floor((damage + 4.45) / 2.235);
    }

    /**
     * Computes the shield stuns for an array of damages.
     * @param {number[]} damages
     * @returns {number[]}
     */
    _computeShieldStuns(damages) {
        const shieldStuns = [];
        damages.forEach((damage) => {
            shieldStuns.push(this._computeShieldStun(damage));
        });

        return shieldStuns;
    }

    /**
     * Computes the "- on shield" of the move.
     * @param {Object} move
     * @returns {number[]}
     */
    _computeOnShield(move) {
        const damages = move.baseDamages;

        const shieldStuns = this._computeShieldStuns(damages);
        const onShields = [];

        shieldStuns.forEach((shieldStun) => {
            onShields.push((move.totalFrames - move.start - shieldStun) * -1);
        });

        return onShields;
    }

    /**
     * Displays the startup frames of the provided move.
     * @param {Object} move Move that will have his startup frames displayed.
     */
    _displayFrameStartup(move) {
        return (
            <View
                style={[
                    styles.frame_startup_container,
                    styles.info_container,
                    { borderColor: this.props.currentTheme.colors.primary },
                ]}
            >
                <Text style={styles.text_info}>{`${move.start} Frame Startup`}</Text>
            </View>
        );
    }

    _displayOnShield(moves) {
        const onShields = this._computeOnShield(moves);

        return (
            <View
                style={[
                    styles.on_shield_container,
                    styles.info_container,
                    { borderColor: this.props.currentTheme.colors.primary },
                ]}
            >
                <Text style={styles.text_info}>{`${onShields.join("/")} On Shield`}</Text>
            </View>
        );
    }

    _displayActiveFrames(move) {
        return (
            <View
                style={[
                    styles.active_frames_container,
                    styles.info_container,
                    { borderColor: this.props.currentTheme.colors.primary },
                ]}
            >
                <Text style={styles.text_info}>{`Active on ${move.start}-${move.end}`}</Text>
            </View>
        );
    }

    _displayTotalFrames(move) {
        const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];
        return (
            <View style={moreInfoStyle}>
                <Text style={styles.text_info}>{`${move.totalFrames} Total Frames`}</Text>
            </View>
        );
    }

    _displayShieldStun(move) {
        const damages = move.baseDamages;
        const shieldStuns = this._computeShieldStuns(damages);
        const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];

        return (
            <View style={moreInfoStyle}>
                <Text style={styles.text_info}>{`${shieldStuns.join("/")} Frames Shield Stun`}</Text>
            </View>
        );
    }

    _displayBaseDamage(move) {
        const damages = move.baseDamages;

        // Don't display if we have no damages
        if (damages.length < 1) {
            return;
        }

        const displayDamage = [];

        // Add the % at the end of the damage
        damages.forEach((damage) => {
            displayDamage.push(`${damage}%`);
        });

        const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];

        return (
            <View style={moreInfoStyle}>
                <Text style={styles.text_info}>{`${displayDamage.join("/")} Base Damage`}</Text>
            </View>
        );
    }

    _displayLandingLag(move) {
        // if (move.hasOwnProperty("landingLag") && move.hasOwnProperty("lcancelledLandingLag")) {
        //     const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];
        //     return (
        //         <View style={moreInfoStyle}>
        //             <Text
        //                 style={styles.text_info}
        //             >{`${move.landingLag} Frames Landing Lag (${move.lcancelledLandingLag} L-canceled)`}</Text>
        //         </View>
        //     );
        // }
    }

    /**
     * Displays the name of the move in a pretty way.
     * @param {String} moveName Name of the move to display the name of.
     */
    _displayPrettyMoveName(moveName) {
        let prettyName = generalMovesName[moveName];
        let splitName = moveName.split("_")[0];
        let characterSpecificName = movesName[this.props.characterName][splitName];
        if (typeof characterSpecificName != "undefined" && prettyName !== characterSpecificName) {
            prettyName += ` - ${characterSpecificName}`;
        }

        return (
            <View>
                <Text style={[styles.move_name, styles.text_info]}>{prettyName}</Text>
            </View>
        );
    }

    render() {
        const theme = this.props.currentTheme;
        const move = this.props.move;
        /**
         * @type {String}
         */
        const moveName = this.props.moveName;

        return (
            <View
                style={[
                    styles.main_container,
                    { borderColor: theme.colors.primary, backgroundColor: theme.colors.border },
                ]}
            >
                {this._displayPrettyMoveName(moveName)}

                {this._displayFrameStartup(move)}
                {this._displayOnShield(move)}
                {this._displayActiveFrames(move)}

                {this._displayTotalFrames(move)}
                {this._displayBaseDamage(move)}
                {this._displayShieldStun(move)} 
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        borderStyle: "solid",
        borderWidth: 1,
    },
    text_info: {
        margin: 10,
    },
    move_name: {
        fontSize: 20,
    },
    info_container: {
        borderTopWidth: 1,
        borderStyle: "solid",
    },
    frame_startup_container: {
        backgroundColor: "green",
    },
    on_shield_container: {
        backgroundColor: "red",
    },
    active_frames_container: {
        backgroundColor: "#3c647d",
    },
});

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the theme
    return {
        currentTheme: state.currentTheme,
    };
};

export default connect(mapStateToProps)(SpecialMoveInformations);
