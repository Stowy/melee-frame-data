import React from "react";
import { View, StyleSheet, FlatList } from "react-native";
import { connect } from "react-redux";
import Text from "./Text";
import CharacterItem from "./CharacterItem";
import charactersData from "../Helpers/charactersLoader";
import { Header } from "react-native/Libraries/NewAppScreen";

class CharacterList extends React.Component {
    constructor(props) {
        super(props);
    }

    _displayDetailForCharacter = (character) => {
        this.props.navigation.navigate("CharacterDetail", { character: character, title: character.name });
    };

    render() {
        return (
            <FlatList
                style={styles.list}
                data={charactersData}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({ item }) => (
                    <CharacterItem character={item} displayDetailForCharacter={this._displayDetailForCharacter} />
                )}
            />
        );
    }
}

const styles = StyleSheet.create({
    list: {
        flex: 1,
        padding: 10,
    },
});

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the theme
    return {
        currentTheme: state.currentTheme,
    };
};

export default connect(mapStateToProps)(CharacterList);
