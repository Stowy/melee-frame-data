import React from "react";
import { StyleSheet, View, ImageBackground } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Text from "../Components/Text";
import { connect } from "react-redux";
import { Surface } from 'react-native-paper';

class CharacterIcon extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { displayDetailForCharacter } = this.props;
        const theme = this.props.currentTheme;
        return (
            <TouchableOpacity
                onPress={() => displayDetailForCharacter(this.props.character)}
            >

            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    surface: {
        height: 110,
        margin: 10,
        flex: 1,
        flexDirection: "column"
    },
    character_image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    name_container: {
        justifyContent: "center",
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    character_name: {
        textAlign: "center",
        fontSize: 26,
        fontFamily: "FolkProH"
    },
});

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the theme
    return {
        currentTheme: state.currentTheme,
    };
};

export default connect(mapStateToProps)(CharacterIcon);
