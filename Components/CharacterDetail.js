import React from "react";
import { View, StyleSheet, Image, FlatList } from "react-native";
import { connect } from "react-redux";
import MoveInformations from "./MoveInformations";
import SpecialMoveInformations from "./SpecialMoveInformations";
import { movesList } from "../Helpers/movesList";

class CharacterDetail extends React.Component {
    constructor(props) {
        super(props);
    }

    /**
     * Displays the given move.
     * @param {String} moveName Name of the move to display
     */
    _displayMove(moveName) {
        const character = this.props.route.params.character;
        const move = character.framedata[moveName];

        if (move != null) {
            move.name = moveName;
        }

        const video = character.videos[moveName];

        if (character.framedata[moveName] != null) {
            return (
                <View style={styles.moves_list}>
                    <MoveInformations move={move} moveName={moveName} characterName={character.name} video={video} />
                </View>
            );
        }
    }

    _displaySpecialMove(moveName) {
        const character = this.props.route.params.character;
        if (!character.framedata.hasOwnProperty(moveName)) {
            return;
        }
        const characterName = character.name;
        /**
         * @type {Array} List of specials subactions for this move.
         */
        const move = character.framedata[moveName];

        return (
            <View style={styles.moves_list}>
                <SpecialMoveInformations move={move} moveName={moveName} characterName={characterName} />
            </View>
        );
    }

    render() {
        const character = this.props.route.params.character;

        return (
            <View style={styles.main_container}>
                {/* <View style={styles.image_container}>
                    <AutoHeightImage source={character.image} width={300} />
                </View> */}

                <FlatList
                    style={styles.list}
                    data={movesList}
                    keyExtractor={(item) => item.name}
                    renderItem={({ item: move }) => {
                        if (move.isNormal) {
                            return this._displayMove(move.name);
                        } else {
                            return this._displaySpecialMove(move.name);
                        }
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        // alignItems: "stretch",
    },
    image_container: {
        alignItems: "center",
    },
    moves_list: {
        alignItems: "stretch",
        margin: 5,
    },
    list: {
        flex: 1,
        padding: 5,
    },
});

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the theme
    return {
        currentTheme: state.currentTheme,
    };
};

export default connect(mapStateToProps)(CharacterDetail);
