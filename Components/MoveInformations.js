import React from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { connect } from "react-redux";
import Text from "./Text";
import generalMovesName from "../Helpers/generalMovesName.json";
import movesName from "../Helpers/movesName.json";
import { Video, AVPlaybackStatus } from "expo-av";
import { Surface } from 'react-native-paper';

/**
 * A grab move is any move that can happen during a grab (so not the grab or dashgrab).
 * This list is used to display grab related moves differently.
 */
const grabingMoves = ["pummel", "fthrow", "bthrow", "uthrow", "dthrow"];

class MoveInformations extends React.Component {
    constructor(props) {
        super(props);
        this.player = React.createRef();
    }

    /**
     * Computes the shield stun for the provided damage.
     * @param {number} damage Damage that the move deals.
     * @returns {number}
     */
    _computeShieldStun(damage) {
        return Math.floor((damage + 4.45) / 2.235);
    }

    /**
     * Computes the shield stuns for an array of damages.
     * @param {number[]} damages
     * @returns {number[]}
     */
    _computeShieldStuns(damages) {
        const shieldStuns = [];
        damages.forEach((damage) => {
            shieldStuns.push(this._computeShieldStun(damage));
        });

        return shieldStuns;
    }

    /**
     * Computes the "- on shield" of the move.
     * @param {Object} move
     * @returns {number[]}
     */
    _computeOnShield(move) {
        const lastHitframe = move.hitFrames[move.hitFrames.length - 1];
        const damages = [];
        lastHitframe.hitboxes.forEach((hitboxId) => {
            const hitbox = move.hitboxes[hitboxId];
            if (!damages.includes(hitbox.damage)) {
                damages.push(hitbox.damage);
            }
        });

        const shieldStuns = this._computeShieldStuns(damages);
        const onShields = [];

        shieldStuns.forEach((shieldStun) => {
            onShields.push((move.totalFrames - lastHitframe.start - shieldStun) * -1);
        });

        return onShields;
    }

    /**
     * Gets all the different damages that the move can deal.
     * @param {Object} move
     * @returns {number[]}
     */
    _getMoveDamages(move) {
        const hitboxes = move.hitboxes;
        const damages = [];

        hitboxes.forEach((hitbox) => {
            if (!damages.includes(hitbox.damage)) {
                damages.push(hitbox.damage);
            }
        });

        return damages;
    }

    _getFrameStartup(move) {
        const hitframes = move.hitFrames;
        const startups = [];

        hitframes.forEach((hitframe) => {
            startups.push(hitframe.start);
        });

        startups.sort();

        return startups;
    }

    _displayShieldStun(move) {
        // Don't display this information if it's a grab
        if (
            move.name === "grab" ||
            move.name === "dashgrab" ||
            grabingMoves.includes(move.name) ||
            move.hitFrames.length == 0
        ) {
            return;
        }

        const damages = this._getMoveDamages(move);
        const shieldStuns = this._computeShieldStuns(damages);
        const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];

        return (
            <View style={moreInfoStyle}>
                <Text style={styles.text_info}>{`${shieldStuns.join("/")} Frames Shield Stun`}</Text>
            </View>
        );
    }

    /**
     * Displays the startup frames of the provided move.
     * @param {Object} move Move that will have his startup frames displayed.
     */
    _displayFrameStartup(move) {
        if (move.hitFrames.length == 0) {
            return;
        }

        const startups = this._getFrameStartup(move);

        return (
            <View
                style={[
                    styles.frame_startup_container,
                    styles.info_container,
                    { borderColor: this.props.currentTheme.colors.primary },
                ]}
            >
                <Text style={styles.text_info}>{`${startups.join("/")} Frame Startup`}</Text>
            </View>
        );
    }

    _displayBaseDamage(move) {
        // Don't display this information if it's a grab
        if (move.name === "grab" || move.name === "dashgrab") {
            return;
        }

        const damages = this._getMoveDamages(move);

        // Don't display if we have no damages
        if (damages.length < 1) {
            return;
        }

        const displayDamage = [];

        // Add the % at the end of the damage
        damages.forEach((damage) => {
            displayDamage.push(`${damage}%`);
        });

        const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];
        return (
            <View style={moreInfoStyle}>
                <Text style={styles.text_info}>{`${displayDamage.join("/")} Base Damage`}</Text>
            </View>
        );
    }

    _displayTotalFrames(move) {
        const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];
        return (
            <View style={moreInfoStyle}>
                <Text style={styles.text_info}>{`${move.totalFrames} Total Frames`}</Text>
            </View>
        );
    }

    _displayOnShield(move) {
        // Don't display this information if it's a grab / grabing move or if it doesn't have hitframes
        if (
            move.name === "grab" ||
            move.name === "dashgrab" ||
            grabingMoves.includes(move.name) ||
            move.hitFrames.length == 0
        ) {
            return;
        }

        const onShields = this._computeOnShield(move);

        return (
            <View
                style={[
                    styles.on_shield_container,
                    styles.info_container,
                    { borderColor: this.props.currentTheme.colors.primary },
                ]}
            >
                <Text style={styles.text_info}>{`${onShields.join("/")} On Shield`}</Text>
            </View>
        );
    }

    _displayActiveFrames(move) {
        const activeFrames = [];
        move.hitFrames.forEach((hitframe) => {
            activeFrames.push(`${hitframe.start}-${hitframe.end}`);
        });

        // Don't display if we have no active frames
        if (activeFrames.length < 1) {
            return;
        }

        return (
            <View
                style={[
                    styles.active_frames_container,
                    styles.info_container,
                    { borderColor: this.props.currentTheme.colors.primary },
                ]}
            >
                <Text style={styles.text_info}>{`Active on ${activeFrames.join("/")}`}</Text>
            </View>
        );
    }

    _displayLandingLag(move) {
        if (move.hasOwnProperty("landingLag") && move.hasOwnProperty("lcancelledLandingLag")) {
            const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];
            return (
                <View style={moreInfoStyle}>
                    <Text
                        style={styles.text_info}
                    >{`${move.landingLag} Frames Landing Lag (${move.lcancelledLandingLag} L-canceled)`}</Text>
                </View>
            );
        }
    }

    _displayAutoCancel(move) {
        if (move.hasOwnProperty("autoCancelBefore") && move.hasOwnProperty("autoCancelAfter")) {
            const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];
            return (
                <View style={moreInfoStyle}>
                    <Text
                        style={styles.text_info}
                    >{`Won't Auto Cancel Frames ${move.autoCancelBefore}-${move.autoCancelAfter}`}</Text>
                </View>
            );
        }
    }

    _displayIASA(move) {
        if (move.iasa != null) {
            const moreInfoStyle = [styles.info_container, { borderColor: this.props.currentTheme.colors.primary }];
            return (
                <View style={moreInfoStyle}>
                    <Text style={styles.text_info}>{`IASA Frame ${move.iasa}`}</Text>
                </View>
            );
        }
    }

    /**
     * Displays the name of the move in a pretty way.
     * @param {String} moveName Name of the move to display the name of.
     */
    _displayPrettyMoveName(moveName) {
        let prettyName = generalMovesName[moveName];
        let splitName = moveName.split("_")[0];
        let characterSpecificName = movesName[this.props.characterName][splitName];
        if (typeof characterSpecificName != "undefined" && prettyName !== characterSpecificName) {
            prettyName += ` - ${characterSpecificName}`;
        }

        return (
            <View>
                <Text style={[styles.move_name, styles.text_info]}>{prettyName}</Text>
            </View>
        );
    }

    _displayMoveVideo() {
        // Check if we have a video of this move
        if (this.props.video == undefined || this.props.video == null) {
            return;
        }

        return (
            <View style={styles.video_container}>
                <Video
                    ref={this.player}
                    source={this.props.video}
                    style={styles.video}
                    resizeMode="cover"
                    isLooping
                    onLoad={(playbackStatus) => {
                        if (!playbackStatus.isPlaying) {
                            this.player.current.playAsync();
                        }
                    }}
                />
            </View>
        );
    }

    render() {
        const { move } = this.props;
        const surfaceStyle = [styles.main_container, { borderColor: this.props.currentTheme.colors.primary }];

        return (
            <Surface style={surfaceStyle} >
                {this._displayPrettyMoveName(move.name)}
                {this._displayFrameStartup(move)}
                {this._displayOnShield(move)}
                {this._displayActiveFrames(move)}
                {this._displayMoveVideo()}
                {this._displayTotalFrames(move)}
                {this._displayBaseDamage(move)}
                {this._displayShieldStun(move)}
                {this._displayIASA(move)}
                {this._displayLandingLag(move)}
                {this._displayAutoCancel(move)}
            </Surface>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        borderStyle: "solid",
        borderWidth: 1,
    },
    text_info: {
        margin: 10,
    },
    move_name: {
        fontSize: 20,
    },
    info_container: {
        borderTopWidth: 1,
        borderStyle: "solid",
    },
    frame_startup_container: {
        backgroundColor: "green",
    },
    on_shield_container: {
        backgroundColor: "red",
    },
    active_frames_container: {
        backgroundColor: "#3c647d",
    },
    video_container: {
        flex: 1,
        justifyContent: "center",
    },
    video: {
        alignSelf: "center",
        width: Dimensions.get("window").width - 40,
        height: Dimensions.get("window").height * 0.5,
        margin: 10,
    },
});

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the theme
    return {
        currentTheme: state.currentTheme,
    };
};

export default connect(mapStateToProps)(MoveInformations);
