import React from "react";
// import { Text } from "react-native";
import Text from "./Text";
import { StyleSheet } from "react-native";
import * as Linking from "expo-linking";
import { connect } from "react-redux";

class Anchor extends React.Component {
    _handlePress = () => {
        Linking.openURL(this.props.href);
        this.props.onPress && this.props.onPress();
    };

    render() {
        const theme = this.props.currentTheme;
        return (
            <Text {...this.props} onPress={this._handlePress} style={[{ color: theme.colors.primary }, styles.link]}>
                {this.props.children}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    link: {
        textDecorationLine: "underline",
    },
});

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the theme
    return {
        currentTheme: state.currentTheme,
    };
};

export default connect(mapStateToProps)(Anchor);
