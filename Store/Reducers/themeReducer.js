import { DarkTheme as NavigationDarkTheme, DefaultTheme as NavigationDefaultTheme } from "@react-navigation/native";
import { DarkTheme as PaperDarkTheme, DefaultTheme as PaperDefaultTheme } from "react-native-paper";

const customDefaultTheme = {
    ...NavigationDefaultTheme,
    ...PaperDefaultTheme,
    colors: {
        ...NavigationDefaultTheme.colors,
        ...PaperDefaultTheme.colors,
    },
};

const customDarkTheme = {
    ...NavigationDarkTheme,
    ...PaperDarkTheme,
    colors: {
        ...NavigationDarkTheme.colors,
        ...PaperDarkTheme.colors,
    },
};

const initialState = { currentTheme: customDarkTheme };

export default function toggleDarkTheme(state = initialState, action) {
    let nextState;
    switch (action.type) {
        case "TOGGLE_DARK_THEME":
            // Reverse the current theme
            nextState = {
                ...state,
                currentTheme: state.currentTheme.dark ? customDefaultTheme : customDarkTheme,
            };

            return nextState || state;
        default:
            return state;
    }
}
