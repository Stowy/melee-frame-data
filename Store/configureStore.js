import { createStore } from "redux";
import toggleDarkTheme from "./Reducers/themeReducer";

export default createStore(toggleDarkTheme);
