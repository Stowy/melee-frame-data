import React from "react";
import { StyleSheet } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import CharacterList from "../Components/CharacterList";
import CharacterDetail from "../Components/CharacterDetail";
import { Ionicons } from "@expo/vector-icons";
import ionprefix from "../Helpers/ionprefix";
import { connect } from "react-redux";
import { TouchableWithoutFeedback as Touchable } from "react-native-gesture-handler";

const CharactersStack = createStackNavigator();

class CharactersStackNavigator extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <CharactersStack.Navigator>
                <CharactersStack.Screen
                    name="Characters"
                    component={CharacterList}
                    options={{
                        title: "Characters",
                        headerLeft: () => (
                            <Touchable onPress={() => this.props.navigation.openDrawer()} style={styles.drawer_button}>
                                <Ionicons
                                    name={ionprefix + "menu"}
                                    color={this.props.currentTheme.colors.text}
                                    size={25}
                                />
                            </Touchable>
                        ),
                    }}
                />
                <CharactersStack.Screen
                    name="CharacterDetail"
                    component={CharacterDetail}
                    options={({ route }) => ({ title: route.params.title })}
                />
            </CharactersStack.Navigator>
        );
    }
}

const styles = StyleSheet.create({
    drawer_button: {
        marginLeft: 10,
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
});

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the theme
    return {
        currentTheme: state.currentTheme,
    };
};

export default connect(mapStateToProps)(CharactersStackNavigator);
