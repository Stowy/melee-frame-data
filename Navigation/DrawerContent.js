import React from "react";
import { View, StyleSheet } from "react-native";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import { Drawer, TouchableRipple, Switch, useTheme } from "react-native-paper";
import { Ionicons } from "@expo/vector-icons";
import ionprefix from "../Helpers/ionprefix";
import Text from "../Components/Text";
import { connect } from "react-redux";

class DrawerContent extends React.Component {
    constructor(props) {
        super(props);
    }

    _toggleTheme() {
        const action = { type: "TOGGLE_DARK_THEME", value: null };
        this.props.dispatch(action);
    }

    render() {
        const theme = this.props.currentTheme;
        return (
            <View style={{ flex: 1 }}>
                <DrawerContentScrollView {...this.props}>
                    <View style={styles.drawerContent}>
                        <Text style={styles.title}>Melee Frame Data</Text>
                        <Drawer.Section style={styles.drawerContent}>
                            <DrawerItem
                                icon={({ color, size }) => (
                                    <Ionicons name={ionprefix + "list"} size={size} color={theme.colors.placeholder} />
                                )}
                                label="Characters"
                                onPress={() => {
                                    this.props.navigation.navigate("Characters Screen");
                                }}
                                labelStyle={[styles.label, { color: theme.colors.placeholder }]}
                            />
                            <DrawerItem
                                icon={({ color, size }) => (
                                    <Ionicons
                                        name={ionprefix + "information-circle-outline"}
                                        size={size}
                                        color={theme.colors.placeholder}
                                    />
                                )}
                                label="About"
                                onPress={() => {
                                    this.props.navigation.navigate("About Screen");
                                }}
                                labelStyle={[styles.label, { color: theme.colors.placeholder }]}
                            />
                        </Drawer.Section>
                        <Drawer.Section title="Preferences">
                            <TouchableRipple
                                onPress={() => {
                                    this._toggleTheme();
                                }}
                            >
                                <View style={styles.preference}>
                                    <Text>Dark Theme</Text>
                                    <View pointerEvents="none">
                                        <Switch value={this.props.currentTheme.dark} />
                                    </View>
                                </View>
                            </TouchableRipple>
                        </Drawer.Section>
                    </View>
                </DrawerContentScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    drawerSection: {
        marginTop: 15,
    },
    title: {
        marginTop: 15,
        marginLeft: 15,
        marginBottom: 30,
        fontSize: 26,
        fontFamily: "FolkProH",
    },
    preference: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
    label: {
        fontFamily: "FolkProH",
    },
});

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the theme
    return {
        currentTheme: state.currentTheme,
    };
};

export default connect(mapStateToProps)(DrawerContent);
