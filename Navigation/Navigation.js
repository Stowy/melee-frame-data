import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import CharactersStackNavigator from "./CharactersStackNavigator";
import AboutStackNavigator from "./AboutStackNavigator";
import DrawerContent from "./DrawerContent";
import { connect } from "react-redux";
import { Provider as PaperProvider } from "react-native-paper";
import { Platform, StatusBar } from "react-native";

const Drawer = createDrawerNavigator();

class Navigation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let theme = this.props.currentTheme;
        let barStyle = Platform.OS === "ios" && !this.props.currentTheme.dark ? "dark-content" : "light-content";
        return (
            <PaperProvider theme={theme}>
                <NavigationContainer theme={theme}>
                    <StatusBar barStyle={barStyle} />
                    <Drawer.Navigator
                        initialRouteName="Characters Screen"
                        drawerContent={(props) => <DrawerContent {...props} />}
                        drawerStyle={{}}
                    >
                        <Drawer.Screen name="Characters Screen" component={CharactersStackNavigator} />
                        <Drawer.Screen name="About Screen" component={AboutStackNavigator} />
                    </Drawer.Navigator>
                </NavigationContainer>
            </PaperProvider>
        );
    }
}

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the theme
    return {
        currentTheme: state.currentTheme,
    };
};

export default connect(mapStateToProps)(Navigation);
